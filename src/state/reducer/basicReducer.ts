import { Reducer } from 'redux';
import { assertUnreachable } from 'utils/assertUnreachable';
import { AppAction } from '../AppAction';

type State = {
  stack: Array<number>,
  current: number,
  decimal: boolean,
  previousState: State | undefined
};

const initialState: State = {
  stack: [],
  current: 0,
  decimal: false,
  previousState: undefined
};

function roundDecimal(n: number): number{
  return Math.round(n * 10) / 10;
}

function operate(stack: Array<number>, operation: string, current: number): Array<number>{
  var originalStack = stack.slice();  
  if (current != 0) originalStack.push(current);

  var newStack = originalStack.slice();

  var num1 = newStack.pop();
  var num2 = newStack.pop();
  var result;
  if (num1 !== undefined && num2 !== undefined){
    switch (operation){
      case 'SUM':
        result = roundDecimal(num1 + num2);
        return newStack.concat(result);
      case 'SUBSTRACT':
        result = roundDecimal(num1 - num2);
        return newStack.concat(result);
      case 'MULTIPLY':
        result = roundDecimal(num1 * num2);
        return newStack.concat(result);
      case 'DIVIDE':
        if (num2 == 0) return originalStack;
        result = roundDecimal(num1 / num2);
        return newStack.concat(result);
    }
  }
  
  const sum = (num1: number, num2: number): number => roundDecimal(num1 + num2);

  if (num1 !== undefined){
    switch (operation){
      case 'ROOT':
        result = roundDecimal(Math.sqrt(num1));
        return newStack.concat(result);
      case 'SUMMATION':
        return [originalStack.reduce(sum)];
    }
  }

  return originalStack;
}

export const basicReducer: Reducer<State, AppAction> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case 'ADD_NUMBER_TO_DISPLAY':
      var decimalPoint = "";
      if (state.decimal) decimalPoint = ".";
      console.log(decimalPoint);
      return {
        stack: state.stack,
        current: parseFloat(state.current.toString() + decimalPoint + action.value),
        decimal: false,
        previousState: state
      }
      case 'FUNCTION_ACTION':
        switch (action.name){
          case 'INTRO':
            return {
              stack: state.stack.concat([roundDecimal(state.current)]),
              current: 0,
              decimal: false,
              previousState: state
            }
          case 'DECIMAL':
            return {
              stack: state.stack,
              current: state.current,
              decimal: true,
              previousState: state
            }
          case 'UNDO':
            if (state.previousState == undefined) return state;
            return state.previousState;
          default:
            return {
              stack: operate(state.stack, action.name, state.current),
              current: 0,
              decimal: false,
              previousState: state
            };
        }
        
    default:
      return state;
  }
};
