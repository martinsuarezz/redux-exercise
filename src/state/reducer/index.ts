import { combineReducers } from 'redux';

import { basicReducer } from './basicReducer';
import { higherOrderReducer } from './higherOrderReducer';

export const rootReducer = higherOrderReducer(
  combineReducers({
    first: basicReducer,
    second: basicReducer,
  })
);
