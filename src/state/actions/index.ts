import { changeSample } from './changeSample';
import { addNumberToDisplay } from './addNumberToDisplay';
import { calculatorFunction } from './calculatorFunction';

export { changeSample, addNumberToDisplay, calculatorFunction};
