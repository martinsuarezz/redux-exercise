type calculatorFunctionAction = {
  type: "FUNCTION_ACTION";
  name: string
};

export const calculatorFunction = (name: string): calculatorFunctionAction => ({
  type: 'FUNCTION_ACTION',
  name: name
});
