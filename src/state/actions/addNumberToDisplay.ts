type AddNumberDisplayAction = {
  type: 'ADD_NUMBER_TO_DISPLAY';
  value: number;
};

export const addNumberToDisplay = (n: number): AddNumberDisplayAction => ({
  type: 'ADD_NUMBER_TO_DISPLAY',
  value: n
});
